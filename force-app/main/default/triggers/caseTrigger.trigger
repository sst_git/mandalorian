trigger caseTrigger on Case (before insert) {
    for(Case cs : Trigger.New) {
        if (cs.Origin == 'Email') {
            cs = email2Case.processEmail(cs);
        }
    }   
}