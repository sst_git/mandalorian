@isTest
public with sharing class caseCustomLayoutController_TEST {
    @testSetup static void setup() {
        
    }

    @isTest static void testGetFieldSetFields() {
        List<caseCustomLayoutController.FieldSetField> fieldSet = caseCustomLayoutController.getFieldSetFields();
        String expectedFieldSet = '[{"type":"STRING","required":false,"label":"Subject","dbRequired":false,"apiName":"Subject"},{"type":"PICKLIST","required":false,"label":"Status","dbRequired":false,"apiName":"Status"},{"type":"STRING","required":false,"label":"Code","dbRequired":true,"apiName":"Planet__r.Code__c"},{"type":"BOOLEAN","required":false,"label":"successfulScan","dbRequired":false,"apiName":"successfulScan__c"}]';
        System.assertEquals(JSON.serialize(fieldSet), expectedFieldSet, 'Los fieldSet deben de coincidir');
    }
}
