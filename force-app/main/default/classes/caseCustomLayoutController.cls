public with sharing class caseCustomLayoutController {

    @AuraEnabled
    public static LwcResponse mandalorianHTTPRequest(String code, Id caseId){
        LwcResponse res = new LwcResponse();
        String accessCode;
        try {
            accessCode = [SELECT AccessCode__c from emailToCase__mdt WHERE External_id__c =: code].AccessCode__c;
        } catch (QueryException e) {
            res.error = true;
            return res;
        }
        
        HttpRequest httpRequest = new HttpRequest();  
        httpRequest.setEndpoint('https://s4g-mandalorian-server.herokuapp.com/scan/' + EncodingUtil.urlEncode(accessCode, 'UTF-8'));
        httpRequest.setMethod('GET');
        String authorization = 'Basic bWFuZG86MTIzNDU=';
        httpRequest.setHeader('Authorization',authorization);

        try {
            Http http = new Http();   
            HttpResponse httpResponse = http.send(httpRequest);  
            if (httpResponse.getStatusCode() == 200 ) {  
                JSON2Apex response = JSON2Apex.parse(httpResponse.getBody());
                res.found = response.found;

                manageCase(caseId);
                System.debug(response);
            } 
            else {
                res.error = true;
            } 
        }
        catch(Exception ex) {  
            res.error = true;
        } 
        
        return res;
    }

    @AuraEnabled(cacheable = true)
    public static List<FieldSetField> getFieldSetFields()
    {
        List<Schema.FieldSetMember> fieldSetMembers = ((SObject)Type.forName('Case').newInstance()).getSObjectType().getDescribe().FieldSets.getMap().get('emailToCaseFieldSet').getFields();
        List<FieldSetField> fields = new List<FieldSetField>();

        for (Schema.FieldSetMember fieldSetMember : fieldSetMembers)
        {
            FieldSetField fieldSetField = new FieldSetField(fieldSetMember);
            fields.add(fieldSetField);
        }

        System.debug(fields);

        return fields;
    }

    private static void manageCase(Id caseId) {
        Case cs = [SELECT Id, Status FROM Case WHERE Id =: caseId];
        cs.Status = 'Closed';

        if (!Schema.sObjectType.Case.fields.Status.isUpdateable()) {
            return;
        }

        try {
            update cs;   
        } catch (DMLException e) {
            System.debug(e.getMessage());
        }
    }

    // Custom Classes

    public class FieldSetField
    {
        @AuraEnabled
        public Boolean dbRequired;
        @AuraEnabled
        public String apiName;
        @AuraEnabled
        public String label;
        @AuraEnabled
        public Boolean required;
        @AuraEnabled
        public String type;

        public FieldSetField(Schema.FieldSetMember fieldSetMember)
        {
            this.dbRequired = fieldSetMember.dbRequired;
            this.apiName = fieldSetMember.fieldPath;
            this.label = fieldSetMember.label;
            this.required = fieldSetMember.required;
            this.type = String.valueOf(fieldSetMember.getType());
        }
    }

    public class JSON2Apex {
        public Boolean found;
        public Boolean error;
    }

    public static JSON2Apex parse(String json) {
        return (JSON2Apex) System.JSON.deserialize(json, JSON2Apex.class);
    }

    public class LwcResponse {
        @AuraEnabled
        public Boolean found;

        @AuraEnabled
        public Boolean error;

        public LwcResponse() {
            this.found = false;
            this.error = false;
        }
    }
}
