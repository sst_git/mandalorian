public with sharing class casosInterplanetarios {
    @AuraEnabled(cacheable=true)
     public static List<Case> getCasosInterplanetarios() {
         return [SELECT Id, Subject, Status, SuppliedEmail, ContactId FROM Case WHERE isClosed = false ORDER BY CreatedDate DESC LIMIT 5];
     }
 }