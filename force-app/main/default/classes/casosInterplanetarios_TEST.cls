@isTest
public with sharing class casosInterplanetarios_TEST {
    @testSetup static void setup() {
        List<Planet__c> testPlanets = new List<Planet__c>();
        testPlanets.add(new Planet__c(Name = 'Hoth', Code__c = 'testCode1'));

        insert testPlanets;

        List<Case> testCases = new List<Case>();
        testCases.add(new Case(Subject = 'Solicitud de escaneo - Hoth', Origin = 'Email', SuppliedName = 'testName', SuppliedEmail = 'test@test.com', Planet__c = [SELECT Id FROM Planet__c WHERE Name = 'Hoth'].Id));

        insert testCases;
    }

    @isTest static void returnRecords() {
        casosInterplanetarios.getCasosInterplanetarios();
    }
}
