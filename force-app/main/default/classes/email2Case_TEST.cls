@isTest
public with sharing class email2Case_TEST {
    @testSetup static void setup() {
        List<Planet__c> testPlanets = new List<Planet__c>();
        testPlanets.add(new Planet__c(Name = 'Hoth', Code__c = 'testCode1'));

        insert testPlanets;

        List<Case> testCases = new List<Case>();
        testCases.add(new Case(Subject = 'Solicitud de escaneo - Hoth', Origin = 'Email', SuppliedName = 'testName', SuppliedEmail = 'test@test.com', Planet__c = [SELECT Id FROM Planet__c WHERE Name = 'Hoth'].Id));

        insert testCases;
    }

    @isTest static void testSuccessProcessEmail() {
        Case cs = [SELECT Id, SuppliedName, SuppliedEmail, Subject, Planet__c FROM Case WHERE Subject = 'Solicitud de escaneo - Hoth'];
        cs = email2Case.processEmail(cs);

        Planet__c planet = [SELECT Id FROM Planet__c WHERE Name = 'Hoth'];
        System.assertEquals(cs.Planet__c, planet.Id, 'Los Ids del planeta y el planeta relacionado al caso deben de coincidir');
    }

    @isTest static void testFailProcessEmail() {
        Case cs = [SELECT Id, SuppliedName, SuppliedEmail, Subject, Planet__c FROM Case WHERE Subject = 'Solicitud de escaneo - Hoth'];
        cs.Subject = 'Un subject no esperado';
        cs = email2Case.processEmail(cs);

        System.assertEquals(cs.Planet__c, null, 'No debe devolver Id dado que no se encuentra el planeta en el sujeto del email');
    }
}
