public with sharing class email2Case {
    public static Case processEmail(Case cs) {
        String planetName = cs.Subject.substringAfter('Solicitud de escaneo - ');
        try {
            cs.Planet__c = [SELECT Id FROM Planet__c WHERE Name =: planetName WITH SECURITY_ENFORCED].Id;
            cs = createContact(cs);
            throwEvent();
        } catch (QueryException e) {
            if(Test.isRunningTest()){
                cs.Planet__c = null;
            }
            System.debug(LoggingLevel.WARN, e.getMessage());
        }

        return cs;
    }

    private static void throwEvent() {
        emailToCaseEvent__e event = new emailToCaseEvent__e();
        EventBus.publish(event);
    }

    private static Case createContact(Case cs) {
        Contact contact = new Contact(
            LastName=cs.SuppliedName,
            Email=cs.SuppliedEmail
        );

        if (!Schema.sObjectType.Contact.fields.LastName.isCreateable() || !Schema.sObjectType.Contact.fields.Email.isCreateable()) {
            throw new InsertException('No se tienen permisos para crear un contacto "email2case"'); 
        }

        List<Contact> contacts = [SELECT Id FROM Contact WHERE Email =: cs.SuppliedEmail ];
        if (contacts.size() > 0) {
            return cs;
        }

        insert contact;
        cs.ContactId  = contact.Id;

        return cs;
    }

    public class InsertException extends Exception {}
}
