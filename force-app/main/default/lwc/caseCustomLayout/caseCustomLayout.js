import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import getFieldSetFields from '@salesforce/apex/caseCustomLayoutController.getFieldSetFields';
import mandalorianHTTPRequest from '@salesforce/apex/caseCustomLayoutController.mandalorianHTTPRequest';

import CODE_FIELD from '@salesforce/schema/Case.Planet__r.Code__c';
const fields = [CODE_FIELD]

export default class CaseCustomLayout extends LightningElement {
    @api recordId
    @track fields;

    @wire(getFieldSetFields, {})
    wiredGFieldSetFields({ error, data }) {
        if (data) {
            this.fields = data;
            console.log(data);
        }
    };

    @wire(getRecord, { recordId: '$recordId', fields })
    case;

    async handleClick() {
        const code = getFieldValue(this.case.data, CODE_FIELD)
        const result = await mandalorianHTTPRequest({ code: code, caseId: this.recordId })
        console.log(result);

        try {
            if (result.error) {
                const evt = new ShowToastEvent({
                    title: '00101010011',
                    message: 'Estamos teniendo alguna interferencia interplanetaria, por favor inténtelo de nuevo',
                    variant: 'error',
                });
                this.dispatchEvent(evt);
            } else {
                const evt = new ShowToastEvent({
                    title: result.found ? 'Éxito' : 'Ohhh...',
                    message: result.found ? 'Has encontrado a Baby Yoda' : 'Sigue intentándolo',
                    variant: result.found ? 'success' : 'warning',
                });
                this.dispatchEvent(evt);
            }
        } catch (error) {
            console.log(error);
        }


    }
}