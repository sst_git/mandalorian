import { LightningElement ,api,wire,track} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { subscribe } from 'lightning/empApi';
import { refreshApex } from '@salesforce/apex';

import getCasosInterplanetarios from '@salesforce/apex/casosInterplanetarios.getCasosInterplanetarios';

const actions = [
    { label: 'View', name: 'show_details' },
];

export default class CasosInterplanetariosTab extends NavigationMixin(LightningElement) {
    channelName = '/event/emailToCaseEvent__e';
    isSubscribeDisabled = false;
    isUnsubscribeDisabled = !this.isSubscribeDisabled;

    subscription = {};
    
    handleSubscribe() {
        const messageCallback = function(response) {
            return refreshApex(this.wiredCasosInterplanetarios);
        };

        subscribe(this.channelName, -1, messageCallback).then(response => {
            this.subscription = response;
        });
    }
    
    @track columns = [{
            type: 'action',
            typeAttributes: { rowActions: actions },
        },{
            label: 'Asunto',
            fieldName: 'Subject',
            type: 'text',
            sortable: true
        },
        {
            label: 'Estado',
            fieldName: 'Status',
            type: 'text',
            sortable: true
        },
        {
            label: 'Email del contacto',
            fieldName: 'SuppliedEmail',
            type: 'text',
            sortable: true
        },
        {
            label: 'Contacto relacionado',
            fieldName: 'ContactId',
            type: 'text',
            sortable: true
        }
    ];
    @track error;
    @track data ;
    @wire(getCasosInterplanetarios)
    wiredCasosInterplanetarios({
        error,
        data
    }) {
        if (data) {
            this.data = data;
            console.log(JSON.stringify(data, null, '\t'));
        } else if (error) {
            this.error = error;
        }
    }

    handleRowAction(event) {
        const record = event.detail.row;
        console.log(record.Id);
        console.log(JSON.stringify(record));
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: record.Id,
                actionName: 'view'
            }
        });
    }
}
